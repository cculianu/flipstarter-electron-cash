# Flipstarter Electron Cash Plugin

A plugin for Electron Cash that allows you to join crowdsourced funding of projects.

This plugin is licensed under the MIT open source license.

*EXPERMIMENTAL*: Use at own risk.

[Release notes](RELEASE-NOTES.md)

## Download

See [Releases](https://gitlab.com/flipstarter/flipstarter-electron-cash/-/releases)

## Developers

Clone this repo somewhere, then symlink the `flipstarter` folder into `Electron-Cash/plugins`

## Creating a release

Run `contrib/deterministic_zip` to generate a zip file that can be installed as an external plugin.

#### Release process

* Tag a release in gitlab.
* Have multiple individuals check out the tag and generate the zip file.
* Compare hases, they must be the same.
* Have individuals sign the checksum.
* Publish the zip along with checksum and signatures of individuals.

## Tests

To run the tests, you need to have cloned & built the Electron-Cash repo. The tests will look for it at `ELECTRON_CASH_HOME`, or default to `~/Electron-Cash/` if the environment variable is not set.

To run tests, run
```
python3 -m unittest discover
```

### Building Electron Cash

- cd ~
- `git clone https://github.com/Electron-Cash/Electron-Cash.git`
- Build Electron-Cash (see [README.rst](https://github.com/Electron-Cash/Electron-Cash/blob/master/README.rst))
